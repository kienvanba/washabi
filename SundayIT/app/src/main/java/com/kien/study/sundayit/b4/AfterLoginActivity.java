package com.kien.study.sundayit.b4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kien.study.sundayit.R;

public class AfterLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);
        ((TextView) findViewById(R.id.tv_email)).setText(getIntent().getStringExtra("email"));
        ((TextView) findViewById(R.id.tv_pwd)).setText(getIntent().getStringExtra("pwd"));
    }
}
