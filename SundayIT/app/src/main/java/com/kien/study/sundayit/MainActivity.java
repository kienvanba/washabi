package com.kien.study.sundayit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kien.study.sundayit.b3.Ex1Activity;
import com.kien.study.sundayit.b3.Ex2Activity;
import com.kien.study.sundayit.b4.Test1Activity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rcvLessons;
    private ArrayList<String> lessons;
    private LessonAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rcvLessons = (RecyclerView) findViewById(R.id.rcv_lessons);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        lessons = new ArrayList<>();
        lessons.add("Bai tap 3 - 1");
        lessons.add("Bai tap 3 - 2");
        lessons.add("Bai tap 4 - test");

        adapter = new LessonAdapter(this, lessons);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvLessons.setAdapter(adapter);
        rcvLessons.setLayoutManager(layoutManager);

        adapter.setItemClickListener(new OnLessonItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent;
                switch (position){
                    case 0:
                        intent = new Intent(MainActivity.this, Ex1Activity.class);
                        break;
                    case 1:
                        intent = new Intent(MainActivity.this, Ex2Activity.class);
                        break;
                    case 2:
                        intent = new Intent(MainActivity.this, Test1Activity.class);
                        break;
                    default:
                        intent = new Intent(MainActivity.this, Ex1Activity.class);
                }
                startActivity(intent);
            }
        });
    }

    interface OnLessonItemClickListener{
        void onClick(int position);
    }

    class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.MyLessonHolder>{
        private Context context;
        private List<String> lessons;
        private OnLessonItemClickListener listener;

        LessonAdapter(Context context, List<String> items){
            this.context = context;
            this.lessons = items;
        }

        @Override
        public MyLessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.layout_lesson_item, parent, false);
            return new MyLessonHolder(v);
        }

        @Override
        public void onBindViewHolder(MyLessonHolder holder, final int position) {
            holder.tvLesson.setText(lessons.get(position));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return lessons.size();
        }

        void setItemClickListener(OnLessonItemClickListener listener){
            this.listener = listener;
        }

        class MyLessonHolder extends RecyclerView.ViewHolder{
            private TextView tvLesson;
            MyLessonHolder(View itemView){
                super(itemView);
                tvLesson = (TextView) itemView.findViewById(R.id.tv_lesson_name);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Main", "on pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Main", "on stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Main", "on destroy");
    }
}
