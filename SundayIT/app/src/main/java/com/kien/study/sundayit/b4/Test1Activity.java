package com.kien.study.sundayit.b4;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.kien.study.sundayit.MainActivity;
import com.kien.study.sundayit.R;

/**
 * Created by kienvanba on 10/8/17.
 */

public class Test1Activity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = Test1Activity.class.getSimpleName();
    private Button btnCall, btnMap, btnPage, btnLogin, btnStartService, btnStopService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1_bt4);
        makeLog("on create");

        btnCall = (Button) findViewById(R.id.btn_call);
        btnMap = (Button) findViewById(R.id.btn_map);
        btnPage = (Button) findViewById(R.id.btn_page);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnStartService = (Button) findViewById(R.id.btn_start_service);
        btnStopService = (Button) findViewById(R.id.btn_stop_service);

        btnCall.setOnClickListener(this);
        btnMap.setOnClickListener(this);
        btnPage.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnStartService.setOnClickListener(this);
        btnStopService.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        makeLog("on start");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        makeLog("on restart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        makeLog("on resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        makeLog("on pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        makeLog("on stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        makeLog("on destroy");
    }

    private ServiceConnection connection;

    @Override
    public void onClick(View v) {
        Intent intent=null;
        switch (v.getId()){
            case R.id.btn_call:
                Uri number = Uri.parse("tel:091447189");
                intent = new Intent(Intent.ACTION_DIAL, number);
                break;
            case R.id.btn_map:
                Uri location = Uri.parse("location lat lng");
                intent = new Intent(Intent.ACTION_VIEW, location);
                break;
            case R.id.btn_page:
                Uri page = Uri.parse("http://www.9gag.com/");
                intent = new Intent(Intent.ACTION_VIEW, page);
                break;
            case R.id.btn_login:
                intent = new Intent(Test1Activity.this, LoginActivity.class);
                break;
            case R.id.btn_start_service:
                connection = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        makeLog("service connect");
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                        makeLog("service disconnect");
                    }
                };
                bindService(new Intent(Test1Activity.this, MyService.class), connection,
                        BIND_AUTO_CREATE);
//                startService(new Intent(Test1Activity.this, MyService.class));
                break;
            case R.id.btn_stop_service:
                unbindService(connection);
                stopService(new Intent(Test1Activity.this, MyService.class));
                break;
            default:
                intent = new Intent(Test1Activity.this, MainActivity.class);
                break;
        }
        if(intent!=null) {
            startActivity(intent);
        }
//        startActivity(Intent.createChooser(intent, "choose"));
    }

    private void makeLog(String message){
        Log.d(TAG, message);
    }
}
