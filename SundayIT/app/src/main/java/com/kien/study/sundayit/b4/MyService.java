package com.kien.study.sundayit.b4;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by kienvanba on 10/8/17.
 */

public class MyService extends Service {
    private static final String TAG = MyService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        makeLog("on create service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        makeLog("start on command");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        makeLog("on destroy");
        super.onDestroy();
    }

    private void makeLog(String mess){
        Log.d(TAG, mess);
    }
}