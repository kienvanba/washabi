package com.kien.study.sundayit.b3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.kien.study.sundayit.R;

/**
 * Created by kienvanba on 10/8/17.
 */

public class Ex2Activity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2_b3);
    }
}
